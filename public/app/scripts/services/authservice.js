'use strict';

/**
 * @ngdoc service
 * @name publicApp.authService
 * @description
 * # authService
 * Service in the publicApp.
 */
angular.module('publicApp')
  .factory('AuthService',
    ['$q', '$timeout', '$http',
      function ($q, $timeout, $http) {
        var user = null;

        $http.get('/confirm-login')
          .success(function (data) {
            if (data) {
              user = data;
            }
          });
        // create user variable


        // return available functions for use in controllers
        return ({
          isLoggedIn: isLoggedIn,
          getUserStatus: getUserStatus,
          login: login,
          logout: logout,
          register: register
        });

        function isLoggedIn() {
          if(user) {
            return true;
          } else {
            return false;
          }
        }

        function getUserStatus() {
          return user;
        }

        function login(credentials) {

          // create a new instance of deferred
          var deferred = $q.defer();

          // send a post request to the server
          $http.post('/users/signin', credentials)
            // handle success
            .success(function (data, status) {
              if(status === 200 && data){
                user = data;
                deferred.resolve();
              } else {
                user = null;
                deferred.reject();
              }
            })
            // handle error
            .error(function (data) {
              user = null;
              deferred.reject();
            });

          // return promise object
          return deferred.promise;

        }

        function logout() {

          // create a new instance of deferred
          var deferred = $q.defer();

          // send a get request to the server
          $http.get('/users/signout')
            // handle success
            .success(function (data) {
              user = null;
              deferred.resolve();
            })
            // handle error
            .error(function (data) {
              user = null;
              deferred.reject();
            });

          // return promise object
          return deferred.promise;

        }

        function register(credentials) {

          // create a new instance of deferred
          var deferred = $q.defer();

          // send a post request to the server
          $http.post('/users/signup', credentials)
            // handle success
            .success(function (data, status) {
              if(status === 200 && data){

                deferred.resolve(data);
              } else {
                deferred.reject();
              }
            })
            // handle error
            .error(function (data) {
              deferred.reject();
            });

          return deferred.promise;

        }

      }
    ]
  );
