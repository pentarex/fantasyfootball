'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:LeagueCtrl
 * @description
 * # LeagueCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
  .controller('LeagueCtrl', function ($scope, $http, $location, AuthService) {

    $scope.user = AuthService.getUserStatus();
    $scope.league = {};
    $scope.league.isPublic = false;
    $scope.selectedUsers = [];
    $scope.myLeagues = [];

    $http.get('/users')
      .success(function (data) {
        $scope.users = data;
      })
      .error(function (data) {
      });

    $http.get('/realLeague')
      .success(function (data) {
        $scope.leagues = data;
        $scope.league.realLeague = $scope.leagues[0];
      })
      .error(function (data) {
      });

    $http.get('/myLeagues')
      .success(function(data){
        $scope.myLeagues = data;
      })
      .error(function(data){

      });


    $scope.addUser = function(user){
      if(typeof user === 'undefined') return; // if empty
      if($scope.selectedUsers.indexOf(user.description) == -1){ // if its not in the array
        $scope.selectedUsers.push({
          _id: user.description._id,
          username: user.description.username,
          points: 0
        });
        $scope.$broadcast('angucomplete-alt:clearInput');
      }

    };

    $scope.removeUser = function(user){
      var index = $scope.selectedUsers.indexOf(user);
      if(index != -1){
        $scope.selectedUsers.splice(index, 1);
      }
    };


    $scope.leagueExtended = function(league){
      $scope.tpl = 'leagueOverview.html';
      $scope.extendedLeague = league;
      $http.get('/predictions/' + $scope.extendedLeague.realLeague.seasonId)
        .success(function(data){
          $scope.predictions = data;
        });

      $http.get('/fixtures/' + $scope.extendedLeague.realLeague.seasonId)
        .success(function(data){
          $scope.fixtures = {};
          angular.forEach(data, function(value, key) {
            if(typeof value.matchDay !== 'undefined'){
              if($scope.fixtures[value.matchDay]){
                $scope.fixtures[value.matchDay][value.fixtureId] = value;
              } else {
                $scope.fixtures[value.matchDay] = {};
                $scope.fixtures[value.matchDay][value.fixtureId] = value;
              }

            }

          });
        });
    };


    $scope.fixtureExtended = function(matchDay){

    };

    $scope.myLeaguesTemplates = function(comingFrom){
      switch(comingFrom) {
        case 'scoreboard':
          $scope.tpl = 'scoreboard.html';
          break;
        case 'predictions':
          $scope.tpl = 'predictions.html';
          break;
        case 'fixtures':
          $scope.tpl = 'fixtures.html';
          break;
        default:
          $scope.tpl = 'leagueOverview.html';
      }

    };


    $scope.create = function(){

      if(typeof $scope.league.isPublic === 'undefined') $scope.league.isPublic = false;
      $scope.league.adminUserId = $scope.user._id;
      $scope.league.participants = $scope.selectedUsers;
      var admin = {
        _id: $scope.user._id,
        username: $scope.user.username,
        points: 0
      };

      $scope.league.participants.push(admin);
      $http.post('/createLeague', $scope.league)
        .success(function(data){
          $location.path('/league/' + data._id);
        })
        .error(function(data){

        });

    };

  });
