'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:AuthenticationClientControllerCtrl
 * @description
 * # AuthenticationClientControllerCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
  .controller('AuthenticationCtrl', function ($scope, $http, $location, AuthService) {
    $scope.authentication = AuthService;

    $scope.signin = function(){
      console.log($scope.credentials);
      AuthService.login($scope.credentials)
        .then(function () {
          $location.path('/');
        }, function(){
          //TODO

        });

    };

    $scope.signup = function(){

      AuthService.register($scope.credentials)
        .then(function (response) {
          $scope.activationToken = response;
        });

    };

    $scope.signout = function(){
      AuthService.logout()
        .then(function () {
          $location.path('/');
        });
    };
  });
