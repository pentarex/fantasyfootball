'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
  .controller('MainCtrl', function ($scope, $http) {

    $scope.getLivescore = function(){

      $scope.champLoading = true;

      $http.get('/livescore')
        .success(function (data) {
          $scope.champLoading = false;
          $scope.champ = data;
        })
        .error(function (data) {
          $scope.champ = data;
        });

    };

    $scope.getLeagues = function(){
      $scope.champLoading = true;
    };

    $scope.getLivescore();



  });

