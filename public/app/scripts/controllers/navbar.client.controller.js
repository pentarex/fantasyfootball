'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:NavbarClientControllerCtrl
 * @description
 * # NavbarClientControllerCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
  .controller('NavBarCtrl', function ($scope, AuthService) {

    $scope.userLoggedIn = function(){
      if(AuthService.isLoggedIn()){
        $scope.user = AuthService.getUserStatus();
      }
      return AuthService.isLoggedIn();
    };

  });
