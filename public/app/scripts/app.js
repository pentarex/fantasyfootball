'use strict';

/**
 * @ngdoc overview
 * @name publicApp
 * @description
 * # publicApp
 *
 * Main module of the application.
 */
angular
  .module('publicApp', [
    'ngAnimate',
    'ngRoute',
    'angucomplete-alt',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/signup', {
        templateUrl: 'views/signup.html',
        controller: 'AuthenticationCtrl'
      })
      .when('/signin', {
        templateUrl: 'views/signin.html',
        controller: 'AuthenticationCtrl'
      })
      .when('/createLeague', {
        templateUrl: 'views/createLeague.html',
        controller: 'LeagueCtrl',
        controllerAs: 'league'
      })
      .when('/myLeagues', {
        templateUrl: 'views/myLeagues.html',
        controller: 'LeagueCtrl',
        controllerAs: 'league'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
