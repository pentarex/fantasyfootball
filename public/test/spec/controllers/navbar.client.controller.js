'use strict';

describe('Controller: NavbarClientControllerCtrl', function () {

  // load the controller's module
  beforeEach(module('publicApp'));

  var NavbarClientControllerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NavbarClientControllerCtrl = $controller('NavbarClientControllerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NavbarClientControllerCtrl.awesomeThings.length).toBe(3);
  });
});
