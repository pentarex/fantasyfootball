# Fantasy Football

Fantasy Football is a game oriented application, which has the purpose to
entertain bored coworkers, friends to play vs each other and to guess
the right football result.

### Why we chose that particular project
We are doing it because we love to compete and we love football.

### Installation
```sh
$ git clone https://pentarex@bitbucket.org/pentarex/fantasyfootball.git
$ cd fantasyfootball
$ cd server
$ npm install
$ npm app.js
```