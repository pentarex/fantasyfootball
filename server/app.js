var express        = require('express'),
	mongoose       = require("mongoose"),
	bodyParser     = require("body-parser"),
	methodOverride = require("method-override"),
	_              = require('lodash'),
	passport	   = require('passport'),
	footballData   = require('./app/utils/football-data'),
    path           = require('path'),
    cookieSession  = require('cookie-session'),
    bodyParser     = require('body-parser'),
    session        = require('express-session');


// Create the application
var app	= express();



// Add Middleware necessary for REST API's
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));

// CORS Support
app.use(function(req, res, next){
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	next();
});


// Connect to MongoDB
mongoose.connect('mongodb://localhost/fantasy-football');
mongoose.connection.once('open', function(){

	// Load the models
	app.models = require('./app/models/models');


    app.use(cookieSession({
        keys: ['secret1', 'secret2']
    }))
    app.use(bodyParser());
    app.use(session({ secret: 'fantasialand' }));

	app.use(passport.initialize());
	app.use(passport.session()); // persistent login sessions
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    var User = mongoose.model('User', app.models.user);
    // Deserialize sessions
    passport.deserializeUser(function(id, done) {
        User.findOne({
            _id: id
        }, '-salt -password', function(err, user) {
            done(err, user);
        });
    });

    // Setting the app router and static folder
    app.use(express.static(path.resolve('./public')));
    //app.use('/bower_components',  express.static(__dirname + './client/bower_components'));
    app.use(express.static(path.resolve('./public/app')));

	// Load the routes
	require('./routes')(app);

	// Send passport for configuration
	require('./app/config/strategies/local')(passport, exports);

	// Call API, scheduled with cron
	footballData.execute(app);

	console.log('Listening on port 3000....');
	app.listen(3000);
});





// Expose app
exports = module.exports = app;
