var cron 		= require('cron'),
	request		= require("request"),
	mongoose 	= require('mongoose'),
    _           = require('lodash'),
	syncRequest = require('sync-request');

var API_KEY = '01c6c5dbf55a4ec28bfb2bd1b783b0ce';

var application;
function FootballData(){
	var self = this;

	self.execute = function(app){
		application = app;
		//populateSeasonInformation();
		// Cron job to call the api.football-data.org at 23:59:59 at 31 of Jan, June, August and December
		// so it can populate the seasons and the teams if there is a change
		var populateTeamsAndSeasons = cron.job("59 59 23 31 0,6,7,11 *", function(){
            //TODO implement something
			populateSeasonInformation();
			console.info('cron job completed');
		});

        var populateFixtures = cron.job("59 59 23 * * *", function(){
            updateFixtures();
        });
		//populateTeamsAndSeasons.start();
        populateFixtures.start();
		//test.start();
	};

}

var populateSeasonInformation = function(){

    var RealLeagueModel = mongoose.model('RealLeague', application.models.realLeague);
    var RealTeamsModel = mongoose.model('RealTeams', application.models.realTeams);
    var RealFixturesModel = mongoose.model('RealFixtures', application.models.realFixtures);

	var res = syncRequest('GET', 'http://api.football-data.org/v1/soccerseasons', {
		  headers: {
			'X-Auth-Token': API_KEY
		 }
	});
	var soccerseasons = JSON.parse(res.getBody('utf8'));
    var seasonIds = [];

    // Saving the leagues into the database
    _.forEach(soccerseasons, function(season) {
        var league = {};
        _.forOwn(season, function(value, key) {
            if(key == 'id') league.seasonId = value;
            if(key == 'caption') league.caption = value;
            if(key == 'numberOfGames') league.numberOfGames = value;
            if(key == 'numberOfTeams') league.numberOfTeams = value;
            if(key == 'year') league.year = value;
            if(key == 'league') league.league = value;
        });
        seasonIds.push(league.seasonId)
        // Saving the leagues into the database
        var leagueModel = new RealLeagueModel(league);
        leagueModel.save(function(err) {});
    });

    // Saving the teams and the fixtures into the database
    _.forEach(seasonIds, function(seasonId) {
        var getTeams = syncRequest('GET', 'http://api.football-data.org/v1/soccerseasons/' + seasonId + '/teams', {
            headers: {
                'X-Auth-Token': API_KEY
            }
        });

        var teamsMap = {};
        var teamObject = JSON.parse(getTeams.getBody('utf8'));
        _.forEach(teamObject.teams, function(teamInfo) {
            var team = {};
            team.seasonId = seasonId;
            team.teamId = teamInfo._links.self.href.split('/')[teamInfo._links.self.href.split('/').length - 1];
            team.name = teamInfo.name;
            team.shortName = teamInfo.shortName;
            team.marketValue = teamInfo.squadMarketValue;
            team.imageUrl = teamInfo.crestUrl;



            var teamModel = new RealTeamsModel(team);

            teamModel.save(function(err) {});
            teamsMap[team.name] = teamModel._id;
        });


        var getFixtures = syncRequest('GET', 'http://api.football-data.org/v1/soccerseasons/' + seasonId + '/fixtures', {
            headers: {
                'X-Auth-Token': API_KEY
            }
        });

        var fixtureObject = JSON.parse(getFixtures.getBody('utf8'));
        _.forEach(fixtureObject.fixtures, function(fixtureInfo) {
            var fixture = {};
            fixture.fixtureId = fixtureInfo._links.self.href.split('/')[fixtureInfo._links.self.href.split('/').length - 1];
            fixture.seasonId = seasonId;
            fixture.status = fixtureInfo.status;
            fixture.matchDay = fixtureInfo.matchday;
            fixture.homeTeamId = teamsMap[fixtureInfo.homeTeamName];
            fixture.awayTeamId = teamsMap[fixtureInfo.awayTeamName];
            fixture.homeTeamName = fixtureInfo.homeTeamName;
            fixture.awayTeamName = fixtureInfo.awayTeamName;
            fixture.date = fixtureInfo.date;
            fixture.goalsHomeTeam = fixtureInfo.result.goalsHomeTeam;
            fixture.goalsAwayTeam = fixtureInfo.result.goalsAwayTeam;


            var fixtureModel = new RealFixturesModel(fixture);
            fixtureModel.save(function(err) {});
        });
        return;
    });

};

var updateFixtures = function(){
    var RealLeagueModel = mongoose.model('RealLeague', application.models.realLeague);
    var RealFixturesModel = mongoose.model('RealFixtures', application.models.realFixtures);

    var leaguePromise = RealLeagueModel.find()
        .exec();
    leaguePromise.then(function(leagues){
            _.forEach(leagues, function(league) {

                var getFixtures = syncRequest('GET', 'http://api.football-data.org/v1/soccerseasons/' + league.seasonId + '/fixtures', {
                    headers: {
                        'X-Auth-Token': API_KEY
                    }
                });

                var fixtureObject = JSON.parse(getFixtures.getBody('utf8'));
                _.forEach(fixtureObject.fixtures, function(fixtureInfo) {
                    var fixture = {};
                    fixture.fixtureId = fixtureInfo._links.self.href.split('/')[fixtureInfo._links.self.href.split('/').length - 1];
                    fixture.seasonId = league.seasonId;
                    fixture.status = fixtureInfo.status;
                    fixture.matchDay = fixtureInfo.matchday;
                    fixture.homeTeamName = fixtureInfo.homeTeamName;
                    fixture.awayTeamName = fixtureInfo.awayTeamName;
                    fixture.date = fixtureInfo.date;
                    fixture.goalsHomeTeam = fixtureInfo.result.goalsHomeTeam;
                    fixture.goalsAwayTeam = fixtureInfo.result.goalsAwayTeam;


                    var fixtureModel = new RealFixturesModel(fixture);
                    fixtureModel._id = undefined;

                    RealFixturesModel.update({fixtureId: fixtureModel.fixtureId}, fixtureModel, {upsert: true}, function(err){
                        if(err){
                            console.log(err);
                        }
                    });
                });


            });



        });



};

module.exports = new FootballData();
