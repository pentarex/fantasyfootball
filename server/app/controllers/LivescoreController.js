/**
 * Created by pentarex on 20/01/16.
 */
var livescore   = require('livescore/lib/livescore-helpers.js');


exports.livescore = function(req, res) {
    livescore.hitLivescore()
        .then(function(body) {
            var result = livescore.parseResponseBody(body);
            res.json(result);
        }, function(error){
            res.json(error);
        });
};