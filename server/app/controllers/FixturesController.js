var mongoose = require('mongoose'),
    _        = require('lodash');


exports.getPredictions = function(req, res, next){
    RealTeams = mongoose.model('RealTeams', req.app.models.realTeams);
    RealFixtures = mongoose.model('RealFixtures', req.app.models.realFixtures);

    var seasonId = req.params.seasonId;

    var week = new Date().getTime() + 168 * 60 * 60 * 1000;
    var todaysDate = new Date().toISOString();
    var afterWeek = new Date(week).toISOString();
    RealFixtures.find({
            seasonId: seasonId,
            //matchDay: 1
            date: { $gt: todaysDate, $lt: afterWeek }
        })
        .populate('homeTeamId')
        .populate('awayTeamId')
        .exec(function(err, fixtures){
            if(err){
                res.json(err)
            } else {
                res.json(fixtures);
            }

        });
};


exports.getFixtures = function(req, res){

    RealTeams = mongoose.model('RealTeams', req.app.models.realTeams);
    RealFixtures = mongoose.model('RealFixtures', req.app.models.realFixtures);

    var seasonId = req.params.seasonId;

    RealFixtures.find({
            seasonId: seasonId
        })
        .populate('homeTeamId')
        .populate('awayTeamId')
        .exec(function(err, fixtures){
            if(err){
                res.json(err)
            } else {
                res.json(fixtures);
            }
        });
};