/**
 * Created by Irfan on 07.12.2015.
 */

var mongoose = require('mongoose'),
	passport = require('passport'),
    jwt = require('jwt-simple');


exports.me = function(req, res, next){
	//TODO
	console.log(req.user);
	next();
};

/**
 * Signup
 */
var secret = 'ThisIsTheFreakinSecret';

exports.signup = function(req, res) {
	User = mongoose.model('User', req.app.models.user);

	// Init Variables
	var user = new User(req.body);

    var payload = { username: user.username };
    var token = jwt.encode(payload, secret);

    user.activated = false;
    user.activationToken = token;

	// Then save the user
	user.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: err
			});
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;
            user.email = undefined;

            res.status(200).send(token);
		}
	});
};

exports.activateUser = function (req, res){
    var token = req.query['token'];
    var User = mongoose.model('User', req.app.models.user);
    var username = jwt.decode(token, secret);

    User.findOneAndUpdate(username, { $set: { activated: true } }, { 'new': true }, function(err, user){
        if(err){

        } else {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;

            req.login(user, function(err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.redirect('/');
                }
            });
        }
    });


};

/**
 * Signin after passport authentication
 */
exports.signin = function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		if (err || !user) {
			res.status(400).send(info);
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

            if(user.activated){
                req.login(user, function(err) {
                    if (err) {
                        res.status(400).send(err);
                    } else {
                        res.json(user);
                    }
                });
            } else {
                res.status(400).send('User is not activated');
            }

		}
	})(req, res, next);
};


exports.getAllUsers = function(req, res){
    User = mongoose.model('User', req.app.models.user);

    User.find({}, '_id username', function(err, users) {

        if(err){
            res.json(err);
        } else {
            //TODO remove all the information, leave only id and username
            res.json(users);
        }
    });

};

/**
 * Signout
 */
exports.signout = function(req, res) {
	req.logout();
	res.redirect('/');
};
