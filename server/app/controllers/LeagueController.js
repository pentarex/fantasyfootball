var mongoose = require('mongoose'),
    _        = require('lodash'),
    passport = require('passport');


exports.create = function(req, res){
    League = mongoose.model('League', req.app.models.league);
    User = mongoose.model('User', req.app.models.user);
    Points = mongoose.model('Points', req.app.models.points);

    var info = req.body;
    //info.realLeagueId = mongoose.Types.ObjectId(info.realLeagueId); // casting to ObjectID
    info.adminUserId = mongoose.Types.ObjectId(info.adminUserId); // casting to ObjectID

    var league = new League(info);

    league.save(function(err){
        if(err){
            res.json(err);
        } else {
            res.json(league);
        }
    });


    var participants = info.participants;
    if(participants.length > 0){
        participants.forEach(function(participant){
            participant._id =  mongoose.Types.ObjectId(participant._id);
            User.findOneAndUpdate(
                {_id: participant._id},
                {$push: {leagues: league._id}},
                {safe: true, upsert: true},
                function(err, user){}
            );

            var points = {
                leagueId: league._id,
                userId: mongoose.Types.ObjectId(participant._id)
            };

            var pointsModel = new Points(points);

            pointsModel.save(function(err){});
        });


    }

};

// Getting Public Leagues
exports.publicLeague = function(req, res){
    League = mongoose.model('League', req.app.models.league);

    League.find({isPublic: true}, function(err, leagues){
        if(!err){
            res.json(leagues);
        }
    });
};


exports.getLeague = function(req, res){
    League = mongoose.model('League', req.app.models.league);

    League.find({_id: req.params.id}, function(err, league){
        if(!err){
            res.json(league);
        }
    });
};


exports.getMyLeagues = function(req, res){
    League = mongoose.model('League', req.app.models.league);
    RealLeague = mongoose.model('RealLeague', req.app.models.realLeague);
    Points = mongoose.model('Points', req.app.models.points);

    var leagues = req.user.leagues;

    // Some promise shit which is very very cool for async tasks
    var leaguePromise = League.find({ _id: { $in: leagues }})
        .populate('realLeague')
        .populate('participants', 'username firstName lastName _id')
        .exec();
    leaguePromise.then(function(leaguesParticipating) {
        //TODO get the points according to the league and the user Or make it in its own controller to get the points
        /**
        var pointsPromise = Points.find({ _id: { $in: leagues }})
            .populate('leagueId')
            .populate('userId', 'username firstName lastName _id')
            .exec();
        */
        res.json(leaguesParticipating);
    });



};




