module.exports = {
	user:				require('./user.model'),
	league: 			require('./league.model'),
	predictions: 		require('./predictions.model'),
	leaderboards: 		require('./leaderboard.model'),
	points: 			require('./points.model'),
	realTeams: 			require('./teams.real.model'),
	realLeague: 		require('./league.real.model'),
	realFixtures: 		require('./fixture.real.model')
};
