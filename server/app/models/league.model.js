var mongoose   = require('mongoose'),
Schema         = mongoose.Schema;

var League = Schema({
	name: {
		type: String,
		required: true
	},
	adminUserId: {
		type: Schema.ObjectId,
		required: true
	},
	realLeague: {
        type: Schema.Types.ObjectId,
        ref: 'RealLeague'
	},
    isPublic: {
        type: Boolean,
        required: true
    },
    participants: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }]
});


module.exports = League;
