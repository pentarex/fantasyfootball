var mongoose   = require('mongoose'),
Schema         = mongoose.Schema;

var Predictions = Schema({
	virtualLeagueId: {
		type: Number,
		required: true
	},
	homeTeamScore: {
		type: Number,
		required: true
	},
	awayTeamScore: {
		type: Number,
		required: true
	},
	fixtureId: {
		type: Number,
		required: true
	},
	userId: {
		type: Number,
		required: true
	}
});


module.exports = Predictions;
