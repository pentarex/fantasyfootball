var mongoose   = require('mongoose'),
Schema         = mongoose.Schema;

var RealLeague = Schema({
	seasonId: {
		type: Number,
		required: true,
        dropDups: true,
        unique: true
	},
	caption: {
		type: String,
		required: true
	},
	league: {
		type: String,
		required: true
	},
	year: {
		type: String,
		required: true
	},
	numberOfTeams: {
		type: Number,
		required: true
	},
	numberOfGames: {
		type: Number,
		required: true
	}
});


module.exports = RealLeague;
