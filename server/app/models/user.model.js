var mongoose   = require('mongoose'),
	Schema     = mongoose.Schema,
	crypto	   = require('crypto');

// Create User Schema
var UserSchema = new Schema({
	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
    email:{
        type: String,
        required: true
    },
	firstName: {
		type: String,
		required: true
	},
	lastName: {
		type: String,
		required: true
	},
	salt: {
		type: String
	},
    activated:{
        type: Boolean
    },
    activationToken:{
        type: String
    },
    leagues: []
});



//Before saving the user
UserSchema.pre('save', function(next) {
	var self = this;
	UserModel.find({'username' : self.username}, function (err, docs) {
		if (!docs.length){
			if (self.password) {
				self.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
				self.password = self.hashPassword(self.password);
				console.log(self.password);
			}
			next();
		}else{
			console.log('user exists: ', docs);
			next(new Error("User exists!"));
		}
	});
});

/**
 * Create instance method for hashing a password
 */
UserSchema.methods.hashPassword = function(password) {
	if (this.salt && password) {
		return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
	} else {
		return password;
	}
};

/**
 * Create instance method for authenticating user
 */
UserSchema.methods.authenticate = function(password) {
	return this.password === this.hashPassword(password);
};

var UserModel = mongoose.model('User', UserSchema);

// Export the model schema
module.exports = UserSchema;
