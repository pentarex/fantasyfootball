var mongoose   = require('mongoose'),
Schema         = mongoose.Schema;

var RealFixtures = Schema({
	fixtureId: {
		type: Number,
		required: true,
        dropDups: true,
        unique: true
	},
	seasonId: {
		type: Number,
		required: true
	},
	date: {
		type: String,
		required: true
	},
	matchDay: {
		type: Number,
		required: true
	},
	homeTeamId: {
        type: Schema.Types.ObjectId,
        ref: 'RealTeams'
	},
	awayTeamId: {
        type: Schema.Types.ObjectId,
        ref: 'RealTeams'
	},
    homeTeamName: {
        type: String,
        required: true
    },
    awayTeamName: {
        type: String,
        required: true
    },
	goalsHomeTeam: {
		type: Number
	},
	goalsAwayTeam: {
		type: Number
	},
	status: {
		type: String,
		required: true
	}
});


module.exports = RealFixtures;
