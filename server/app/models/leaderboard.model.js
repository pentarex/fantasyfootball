var mongoose   = require('mongoose'),
Schema         = mongoose.Schema;

var Leaderboard = Schema({
	userId: {
		type: Number,
		required: true
	},
	points: {
		type: Number,
		required: true
	},
	vLeagueId: {
		type: Number,
		required: true
	}
});


module.exports = Leaderboard;
