var mongoose   = require('mongoose'),
Schema         = mongoose.Schema;

var RealTeams = Schema({
    teamId:{
        type: Number,
        required: true
    },
    seasonId: {
        type: Number,
        required: true
    },
	name: {
		type: String,
		required: true,
        dropDups: true,
        unique: true
	},
	shortName: {
		type: String
	},
	marketValue: {
		type: String
	},
	imageUrl: {
		type: String
	}
});


module.exports = RealTeams;
