var mongoose   = require('mongoose'),
Schema         = mongoose.Schema;

var Points = Schema({
	leagueId: {
        type: Schema.Types.ObjectId,
        ref: 'League'
    },
	userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
	},
    isPending: {
        type: Boolean
    },
    points:{
        type: Number,
        default: 0
    }
});


module.exports = Points;
