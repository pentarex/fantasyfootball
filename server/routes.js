/**
 * Created by Irfan on 07.12.2015.
 */

var users = require('./app/controllers/UserController'),
    realLeague = require('./app/controllers/RealLeagueController'),
    league = require('./app/controllers/LeagueController'),
    fixtures = require('./app/controllers/FixturesController'),
    livescore = require('./app/controllers/LivescoreController');


module.exports = function(app){

    // Setting up the users profile api
    app.route('/users').get(users.getAllUsers);
    app.route('/users/me').get(users.me);
    app.route('/users/signup').post(users.signup);
    app.route('/users/activateAccount').get(users.activateUser);
    app.route('/users/signin').post(users.signin);
    app.route('/users/signout').get(users.signout);
    app.route('/confirm-login').get(function(req, res){
        res.send(req.user);
    });

    app.route('/livescore').get(livescore.livescore);


    app.route('/realLeague').get(realLeague.getRealLeagues);


    app.route('/createLeague').post(league.create);
    app.route('/publicLeagues').get(league.publicLeague);
    app.route('/league/:id').get(league.getLeague);
    app.route('/myLeagues').get(league.getMyLeagues);

    app.route('/fixtures/:seasonId').get(fixtures.getFixtures);
    app.route('/predictions/:seasonId').get(fixtures.getPredictions);
};
